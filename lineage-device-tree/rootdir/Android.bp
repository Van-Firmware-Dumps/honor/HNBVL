//
// Copyright (C) 2024 The LineageOS Project
//
// SPDX-License-Identifier: Apache-2.0
//

// Init scripts
sh_binary {
    name: "change_sensor_access.sh",
    src: "bin/change_sensor_access.sh",
    vendor: true,
}

sh_binary {
    name: "dcc_extension.sh",
    src: "bin/dcc_extension.sh",
    vendor: true,
}

sh_binary {
    name: "dsl_agent.sh",
    src: "bin/dsl_agent.sh",
    vendor: true,
}

sh_binary {
    name: "init.class_main.sh",
    src: "bin/init.class_main.sh",
    vendor: true,
}

sh_binary {
    name: "init.crda.sh",
    src: "bin/init.crda.sh",
    vendor: true,
}

sh_binary {
    name: "init.hn.cota_ncfg_copy.sh",
    src: "bin/init.hn.cota_ncfg_copy.sh",
    vendor: true,
}

sh_binary {
    name: "init.hn.rfnvcust.sh",
    src: "bin/init.hn.rfnvcust.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-cliffs.sh",
    src: "bin/init.kernel.post_boot-cliffs.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-cliffs_2_2_1.sh",
    src: "bin/init.kernel.post_boot-cliffs_2_2_1.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-cliffs_2_3_0.sh",
    src: "bin/init.kernel.post_boot-cliffs_2_3_0.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-cliffs_3_3_1.sh",
    src: "bin/init.kernel.post_boot-cliffs_3_3_1.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-cliffs_default_3_4_1.sh",
    src: "bin/init.kernel.post_boot-cliffs_default_3_4_1.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-pineapple.sh",
    src: "bin/init.kernel.post_boot-pineapple.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-pineapple_2_3_1_1.sh",
    src: "bin/init.kernel.post_boot-pineapple_2_3_1_1.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-pineapple_2_3_2_0.sh",
    src: "bin/init.kernel.post_boot-pineapple_2_3_2_0.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot-pineapple_default_2_3_2_1.sh",
    src: "bin/init.kernel.post_boot-pineapple_default_2_3_2_1.sh",
    vendor: true,
}

sh_binary {
    name: "init.kernel.post_boot.sh",
    src: "bin/init.kernel.post_boot.sh",
    vendor: true,
}

sh_binary {
    name: "init.mdm.sh",
    src: "bin/init.mdm.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.class_core.sh",
    src: "bin/init.qcom.class_core.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.coex.sh",
    src: "bin/init.qcom.coex.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.early_boot.sh",
    src: "bin/init.qcom.early_boot.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.efs.sync.sh",
    src: "bin/init.qcom.efs.sync.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.post_boot.sh",
    src: "bin/init.qcom.post_boot.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.sdio.sh",
    src: "bin/init.qcom.sdio.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.sensors.sh",
    src: "bin/init.qcom.sensors.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.sh",
    src: "bin/init.qcom.sh",
    vendor: true,
}

sh_binary {
    name: "init.qcom.usb.sh",
    src: "bin/init.qcom.usb.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.display_boot.sh",
    src: "bin/init.qti.display_boot.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.graphics.sh",
    src: "bin/init.qti.graphics.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.debug-cliffs.sh",
    src: "bin/init.qti.kernel.debug-cliffs.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.debug-pineapple.sh",
    src: "bin/init.qti.kernel.debug-pineapple.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.debug.sh",
    src: "bin/init.qti.kernel.debug.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.early_debug-pineapple.sh",
    src: "bin/init.qti.kernel.early_debug-pineapple.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.early_debug.sh",
    src: "bin/init.qti.kernel.early_debug.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.kernel.sh",
    src: "bin/init.qti.kernel.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.keymaster.sh",
    src: "bin/init.qti.keymaster.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.media.sh",
    src: "bin/init.qti.media.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.qcv.sh",
    src: "bin/init.qti.qcv.sh",
    vendor: true,
}

sh_binary {
    name: "init.qti.write.sh",
    src: "bin/init.qti.write.sh",
    vendor: true,
}

sh_binary {
    name: "prepare_wifi_cfg.sh",
    src: "bin/prepare_wifi_cfg.sh",
    vendor: true,
}

sh_binary {
    name: "qca6234-service.sh",
    src: "bin/qca6234-service.sh",
    vendor: true,
}

sh_binary {
    name: "system_dlkm_modprobe.sh",
    src: "bin/system_dlkm_modprobe.sh",
    vendor: true,
}

sh_binary {
    name: "vendor_modprobe.sh",
    src: "bin/vendor_modprobe.sh",
    vendor: true,
}

// Init configuration files
prebuilt_etc {
    name: "init.qcom.factory.rc",
    src: "etc/init.qcom.factory.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qcom.rc",
    src: "etc/init.qcom.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qcom.usb.qssi.rc",
    src: "etc/init.qcom.usb.qssi.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qcom.usb.rc",
    src: "etc/init.qcom.usb.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qti.kernel.rc",
    src: "etc/init.qti.kernel.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qti.kernel.test.rc",
    src: "etc/init.qti.kernel.test.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.qti.ufs.rc",
    src: "etc/init.qti.ufs.rc",
    sub_dir: "init/hw",
    vendor: true,
}

prebuilt_etc {
    name: "init.target.rc",
    src: "etc/init.target.rc",
    sub_dir: "init/hw",
    vendor: true,
}

// fstab
prebuilt_etc {
    name: "fstab.hyperhold",
    src: "etc/fstab.hyperhold",
    vendor: true,
}
